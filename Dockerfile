FROM node:9.2.0-alpine
LABEL version="1.0"
LABEL description="nodejs 9.2"

ENV APPDIR /opt/node-test

WORKDIR $APPDIR

ADD . $APPDIR

RUN yarn install

# CMD ["/bin/bash"]  docker exec: \"/bin/bash\": stat /bin/bash: no such file or directory
CMD ["/bin/sh"]