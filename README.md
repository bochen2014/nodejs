# Sample NodeJS Project for GitLab Runner

This is NodeJS project that accompanies my guest
[blog post](https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/)
on Gitlab.com.

It contains two independent modules to be tested with GitLab Runner.

### Running the tests

To run test locally, first start a PostgreSQL database.

To start a db instance with Docker:
```
docker run \
  --name postgres-db \
  --publish=5432:5432 \
  -e POSTGRES_PASSWORD=123456 \
  -e POSTGRES_USER=testuser \
  -d postgres:9.5.0
```

Then the db will be available at the following address:
> postgres://testuser:123456@localhost:5432/postgres

To start the tests:
```
DB_USER=testuser \
DB_PASS=123456 \
DB_HOST=localhost:5432  \
node ./specs/start.js
```
## docker image 
```bash
/media/bochen2014/Work/__work/gitlab-nodejs (master +)$ docker tag   bo-gitlab-nodejs registry.gitlab.com/bochen2014/nodejs

/media/bochen2014/Work/__work/gitlab-nodejs (master +)$ docker push  registry.gitlab.com/bochen2014/nodejs
The push refers to a repository [registry.gitlab.com/bochen2014/nodejs]
9ccc35cc891e: Pushed
29865c3a50bf: Pushed
e8dbc9a95f0e: Pushed
60f41a2b77ff: Pushed
18ebebbb9019: Pushed
6dfaec39e726: Pushed
latest: digest: sha256:c366de8f09e23453f589b18ae56bdda4b37f4d53ef353e9297de14d17efd16ab size: 1578


/media/bochen2014/Work/__work/gitlab-nodejs (master +)$ docker image ls
REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
bo-gitlab-nodejs                        latest              6894db1bd509        14 minutes ago      114MB
registry.gitlab.com/bochen2014/nodejs   latest              6894db1bd509        14 minutes ago      114MB
node                                    9.2.0-alpine        cfca57dfe1bc        3 weeks ago         67.5MB
busybox                                 latest              6ad733544a63        7 weeks ago         1.13MB
```